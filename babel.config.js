module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: process.env.npm_package_engines_node || require('./package.json').engines.node
        },
        useBuiltIns: 'entry',
        corejs: 3,
        ignoreBrowserslistConfig: true
      }
    ]
  ],
  plugins: [
    '@babel/plugin-transform-runtime',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    '@babel/plugin-proposal-optional-chaining',
    ['@babel/plugin-proposal-private-methods', { loose: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }]
  ]
};
