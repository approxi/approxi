export function power(x, radix) {
  const numerator = Math.log(Math.abs(x));
  const denominator = Math.log(Math.abs(radix));
  return Math.floor(numerator / denominator);
};
