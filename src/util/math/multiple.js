import { mostSignificantExponent } from './decimal';

/*
  Given number x and multiplicand m > 0:

  Ceil x to nearest multiple of m:                    ceil(x / m) * m
  Force-ceil x to next multiple of m:                 floor((x / m) + 1) * m

  Floor x to nearest multiple of m:                   floor(x / m) * m
  Force-floor x to next multiple of m:                ceil((x / m) - 1) * m

  Round x to nearest multiple of m towards zero:      sign(x) * floor(abs(x) / m) * m
  Force-round x to next multiple of m towards zero:   sign(x) * ceil((abs(x) / m) - 1) * m

  Round x to nearest multiple of m away from zero:    sign(x) * ceil(abs(x) / m) * m
  Force-round x to next multiple of m away from zero: sign(x) * floor((abs(x) / m) + 1) * m
*/

function defaultMultiplicand(x) {
  return 10 ** mostSignificantExponent(x || 1);
}

function baseMultiple(method = 'ceil', addend = 0, x, multiplicand) {
  multiplicand = multiplicand || defaultMultiplicand(x);
  return Math[method]((x / multiplicand) + addend) * multiplicand;
}

export function ceilToMultipleOf(x, multiplicand, force = false) {
  const method = force ? 'floor' : 'ceil';
  const addend = +!!force; // = force ? 1 : 0;
  return baseMultiple(method, addend, x, multiplicand);
};

export function floorToMultipleOf(x, multiplicand, force = false) {
  const method = force ? 'ceil' : 'floor';
  const addend = -!!force; // = force ? -1 : 0;
  return baseMultiple(method, addend, x, multiplicand);
};

export function roundToMultipleOf(x, multiplicand, force = false, toZero = false) {
  const floor = force ^ toZero;
  const method = floor ? 'floor' : 'ceil';
  const addend = (force & floor) - (force & ~floor); // = force * (floor - !floor);
  return Math.sign(x) * baseMultiple(method, addend, Math.abs(x), multiplicand);
};
