export function mostSignificantExponent(x) {
  return x === 0 ? 0 : Math.floor(Math.log10(Math.abs(x)));
};

export function leastSignificantExponent(x) {
  if (x === Number.POSITIVE_INFINITY || x === Number.NEGATIVE_INFINITY) {
    return Number.NEGATIVE_INFINITY;
  }
  if (!Number.isFinite(x)) {
    return Number.NaN;
  }
  const [coefficient, exponent = ''] = (+x).toExponential().split('e', 2);
  const [, decimals = ''] = coefficient.split('.', 2);
  return (+exponent) - decimals.length;
};

export function greatestCommonExponent(xs) {
  if (!xs?.length) {
    return Number.NaN;
  }
  const reducer = (accumulator, currentValue) =>
    Math.min(accumulator, leastSignificantExponent(currentValue));
  return xs.reduce(reducer, Number.POSITIVE_INFINITY);
};

export function decimalPlaces(x) {
  return Math.max(0, -leastSignificantExponent(x));
};

export function round(x, factor) {
  factor = factor || 10 ** decimalPlaces(x);
  return Math.round(x * factor) / factor;
};

export function isDivisibleBy(a, b) {
  const factor = 10 ** Math.max(decimalPlaces(a), decimalPlaces(b));
  a *= factor;
  b *= factor;
  return (a % (b || 1) === 0);
};
