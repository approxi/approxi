import EventEmitter from 'events';
import r from 'rethinkdb';
import { customAlphabet } from 'nanoid/non-secure';
import debug from 'debug';

const log = debug('server:database');

export default class Database extends EventEmitter {
  #connection = null;
  #options;
  #nanoid = null;

  constructor(options) {
    super();
    this.options = options;
    const { alphabet, length } = this.#options.primaryKey;
    this.#nanoid = customAlphabet(alphabet, length);
  }

  get options() {
    return this.#options;
  }

  set options(options) {
    const { primaryKey } = options;
    this._checkPrimaryKey(primaryKey);
    this.#options = { ...options };
  }

  // https://github.com/babel/babel-eslint/issues/749
  // #checkPrimaryKey(pk) {
  _checkPrimaryKey(pk) {
    if (!pk?.alphabet?.length) {
      throw new TypeError('Primary key alphabet is required');
    }
    if (pk?.length <= 0) {
      throw new RangeError('Primary key length must be positive');
    }
  }

  async connect() {
    if (this.#connection?.isOpen()) {
      return;
    }
    this.#connection = await r.connect(this.#options);
    log('connected');
    this.#connection.use(this.#options.db);
    for (const event of ['close', 'timeout', 'error']) {
      this.#connection.on(event, this._droppedConnectionHandler(event));
    }
    log('setup');
    try {
      this.createDatabase(this.#options.db);
    } catch (error) {
      log('setup error');
      await this.disconnect();
      throw error;
    }
    this.emit('connect');
  }

  // https://github.com/babel/babel-eslint/issues/749
  // #droppedConnectionHandler(event) {
  _droppedConnectionHandler(event) {
    return async () => {
      log(`connection ${event}`);
      await this.disconnect();
    };
  }

  async disconnect() {
    if (this.#connection?.isOpen()) {
      await this.#connection.close();
      this.emit('disconnect');
    }
    this.#connection = null;
  }

  // https://github.com/babel/babel-eslint/issues/749
  // #throwResultError(result) {
  _throwResultError(result) {
    if (result.errors) {
      throw result.first_error;
    }
  }

  async createDatabase(name) {
    log('createDatabase', name);
    await this.connect();
    try {
      return await r.dbCreate(name).run(this.#connection);
    } catch (error) {
      if (error instanceof r.Error.ReqlRuntimeError) {
        log(`database ${name} exists`);
      } else {
        throw error;
      }
    }
  }

  async createTable(name) {
    log('createTable', name);
    await this.connect();
    try {
      return await r.tableCreate(name).run(this.#connection);
    } catch (error) {
      if (error instanceof r.Error.ReqlOpFailedError) {
        log(`table ${name} exists`);
      } else {
        throw error;
      }
    }
  }

  async getDoc(tableName, docId, excludedProperties) {
    log('getDoc', tableName, docId, '-', excludedProperties);
    await this.connect();
    // use {} as an empty placeholder if `get` returns null, which `without`
    // refuses to be applied to
    let cmd = r.table(tableName).get(docId).default({});
    if (excludedProperties) {
      cmd = cmd.without(excludedProperties);
    }
    const doc = await cmd.run(this.#connection);
    return doc.id ? doc : null;
  }

  async insertDoc(tableName, doc = {}) {
    log('insertDoc', tableName, doc);
    await this.connect();
    const proxy = {};
    let result;
    do {
      proxy.id = doc.id || this.#nanoid();
      result = await r.table(tableName).insert({ ...doc, ...proxy }).run(this.#connection);
    } while (
      result.errors &&
      result.first_error.startsWith('Duplicate primary key') &&
      !doc.id
    );
    this._throwResultError(result);
    log(`inserted ${proxy.id}`, result);
    return proxy.id;
  }

  async deleteDoc(tableName, docId) {
    log('deleteDoc', tableName, docId);
    await this.connect();
    const result = await r.table(tableName).get(docId).delete().run(this.#connection);
    this._throwResultError(result);
    log(`deleted ${docId}`, result);
  }

  async updateDocOptimistically(tableName, docId, updateCallback, lockProperty = '_lock', excludedProperties = []) {
    log('updateDoc', tableName, docId);
    await this.connect();
    const withoutProps = [lockProperty, ...excludedProperties];
    let doc;
    let updateResult;
    do {
      doc = await this.getDoc(tableName, docId);
      if (!doc) {
        return null;
      }
      doc = updateCallback(doc);
      const lock = doc[lockProperty] || 0;
      const cas = latestDoc => {
        return r.branch(latestDoc(lockProperty).default(0).eq(lock),
          r(doc).merge({
            [lockProperty]: lock === Number.MAX_SAFE_INTEGER ? 0 : lock + 1
          }),
          {}
        );
      };
      const cmd = r.table(tableName).get(docId).update(cas);
      updateResult = await cmd.run(this.#connection);
    } while (updateResult.unchanged);
    this._throwResultError(updateResult);
    for (const prop of withoutProps) {
      delete doc[prop];
    }
    log(`updated ${doc.id}`, updateResult);
    return doc;
  }

  async subscribeToTableChanges(tableName, callback, excludedProperties) {
    await this.connect();
    let query = r.table(tableName);
    if (excludedProperties) {
      query = query.without(excludedProperties);
    }
    const cursor = await query.changes({ includeTypes: true }).run(this.#connection);
    log(`changefeed subscription started for table ${tableName}`, '-', excludedProperties);
    cursor.on('data', callback);
    cursor.on('error', error => {
      log('subscription cursor error', error);
    });
    this.once('connect', () => {
      log(`resubscribe to changefeed for table ${tableName}`);
      this.subscribeToTableChanges(tableName, callback, excludedProperties);
    });
  }
};
