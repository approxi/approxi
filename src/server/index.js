import 'dotenv/config';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import runner from 'systemic-service-runner';
import debug from 'debug';
import system from './system';

const log = debug('server');
const logger = { error: log, info: log };
const service = runner(system(), { logger });

async function onServiceStart(error, components) {
  if (error) {
    log('error starting service: ', error);
    await service.stop();
    process.exit(1);
  }
  log('service started');
};

service.start(onServiceStart);
