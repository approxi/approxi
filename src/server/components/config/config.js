import cloneDeep from 'lodash/cloneDeep';
import config from '../../../config/server';

export default () => ({
  async start() {
    return cloneDeep(config);
  }
});
