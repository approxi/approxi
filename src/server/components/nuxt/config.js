import cloneDeep from 'lodash/cloneDeep';
import nuxtConfig from '../../../config/nuxt';

export default () => ({
  async start() {
    return cloneDeep(nuxtConfig);
  }
});
