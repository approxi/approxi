import Model from '../../database/model';
import schema from '../../../schemas/surveys';

export default () => ({
  async start({ database }) {
    const surveys = new Model(database, 'surveys', schema);
    await surveys.init();
    return surveys;
  }
});
