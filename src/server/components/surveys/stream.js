import changeStream from '../../streams/model-changes';

export default () => ({
  async start({ server: { ws }, models: { stats } }) {
    return changeStream(ws.of('/surveys/stats'), stats);
  }
});
