import io from 'socket.io';
import debug from 'debug';

export default () => {
  let socket;

  const start = async function start({ server: { http }, config }) {
    socket = io(http, config);
    return socket;
  };

  const stop = async function stop() {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line no-unused-expressions
      socket?.close(() => {
        socket = null;
        resolve();
      });
    });
  };

  return {
    start,
    stop
  };
};
