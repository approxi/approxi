import debug from 'debug';

export default (socketNamespace, model) => {
  const log = debug(`streams${socketNamespace.name.replace(/\//g, ':')}`);

  socketNamespace.on('connect', socket => {
    log(`client ${socket.id} connected`);
    socket.on('disconnect', reason => {
      log(`client ${socket.id} disconnected: ${reason}`);
    });
    socket.on('error', error => {
      log(`client ${socket.id} error`, error);
    });
    socket.on('join', async (docId, callback) => {
      let doc;
      try {
        doc = await model.get(docId);
      } catch (error) {
        log(`error getting doc ${docId}`, error);
      }
      if (!doc) {
        return;
      }
      log(`client ${socket.id} joins ${docId}`);
      socket.join(docId);
      callback(doc);
    });
  });

  model.subscribe(({ old_val: oldDoc, new_val: newDoc, type } = {}) => {
    if (type !== 'change' || !oldDoc || !newDoc) {
      return;
    }
    log(`emit change to ${newDoc.id}`, newDoc);
    socketNamespace.to(newDoc.id).volatile.emit('change', newDoc);
  });

  return socketNamespace;
};
