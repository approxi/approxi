export default ({ $axios, error }) => {
  $axios.onError(err => {
    let statusCode;
    let message;
    if (err.response) {
      statusCode = err.response.status;
      message = err.response.statusText;
    } else if (err.request) {
      statusCode = 408;
    } else {
      message = err.message;
    }
    error({ statusCode, message });
    return Promise.reject(err);
  });
};
