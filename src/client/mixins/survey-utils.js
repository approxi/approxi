export default {
  methods: {
    anchorBound(range /* can be Array or Object */) {
      const l = range[0];
      const r = range[1];
      // TODO: This is duplicated in Slider and SurveyViewer
      if (l <= 0 && r >= 0) { // crossing 0
        return 0;
      } else if (l < 0) { // negative
        return Math.max(
          Number.isFinite(l) ? l : r,
          Number.isFinite(r) ? r : l
        );
      } else { // positive
        return Math.min(
          Number.isFinite(l) ? l : r,
          Number.isFinite(r) ? r : l
        );
      }
    }
  }
};
