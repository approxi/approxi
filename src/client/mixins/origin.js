export default {
  methods: {
    origin() {
      return window.location.origin;
    },
    hostname() {
      return window.location.hostname;
    }
  }
};
