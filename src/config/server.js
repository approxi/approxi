const HOST = process.env.HOST || process.env.API_HOST || process.env.npm_package_config_host;
const PORT = process.env.PORT || process.env.API_PORT || process.env.npm_package_config_port;

const app = {
  'case sensitive routing': false,
  'strict routing': false,
  'trust proxy': false,
  'x-powered-by': false,
  middleware: {
    cors: {
      origin: HOST || '*',
      methods: ['GET', 'HEAD', 'POST']
    },
    helmet: {
      contentSecurityPolicy: false,
      // dnsPrefetchControl: {
      //   allow: true
      // },
      dnsPrefetchControl: false,
      expectCt: false,
      frameguard: false,
      // hidePoweredBy: true,
      hsts: false,
      noSniff: false,
      permittedCrossDomainPolicies: false,
      referrerPolicy: false
      // xssFilter: true
    }
  }
};

const server = {
  // see Node Net.Server.listen(options[, callback])
  http: {
    host: HOST,
    port: PORT || 3000
  },
  // see https://socket.io/docs/server-api/#new-Server-httpServer-options
  ws: {
    origins: `${HOST || '*'}:${PORT || '*'}`,
    serveClient: false
  }
};

const database = {
  // see https://rethinkdb.com/api/javascript/connect
  host: process.env.DATABASE_HOST, // default: 'localhost'
  port: process.env.DATABASE_PORT, // default: 28015
  user: process.env.DATABASE_USER, // default: 'admin'
  password: process.env.DATABASE_PASS, // default: ''
  db: process.env.DATABASE_NAME || 'approxi', // default: 'test'
  ssl: null, // default: null
  primaryKey: {
    alphabet: 'sOwnPropMN49CEiqhXvHJdSymlFURTag61GQfuD8YIWz2Zk5xKB7LV30Abject',
    length: 12
  }
};

export default {
  app,
  server,
  database
};
