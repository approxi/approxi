import 'dotenv/config'; // Make sure environment variables are imported when run via `nuxt build`
import serverConfig from './server';

export default {
  dev: (process.env.NODE_ENV !== 'production'),
  mode: 'universal',
  env: {
    // Make ID format available to components, compiled at build-time
    DATABASE_PRIMARY_KEY_FORMAT: `[${serverConfig.database.primaryKey.alphabet}]{${serverConfig.database.primaryKey.length}}`
  },
  head: {
    titleTemplate: `%s – ${process.env.npm_package_name || ''}`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  loading: {
    color: '#0cf'
  },
  css: [
    '~assets/stylesheets/scaffolding/main.less'
  ],
  plugins: [
    '~plugins/axios.js',
    '~plugins/linkify.js'
  ],
  buildModules: [
    '@nuxtjs/eslint-module'
  ],
  modules: [
    '@nuxtjs/axios'
    // '@nuxtjs/component-cache'
  ],
  axios: {
    // Default base URLs compiled during build
    // see https://axios.nuxtjs.org/options
    // Server => [API_]HOST:[API_]PORT/api/ (default: http://localhost:3000/api/)
    prefix: '/api/',
    // Client => /api/
    browserBaseURL: '/api/'
  },
  build: {
    extractCss: true,
    babel: {
      presets({ isServer }, [_, { buildTarget }], options = {}) {
        const envTargets = {
          client: { browsers: process.env.npm_package_browserslist || 'defaults' },
          server: { node: process.env.npm_package_engines_node || '*' }
        };
        return [
          [
            '@nuxt/babel-preset-app',
            {
              ...options,
              targets: envTargets[buildTarget],
              useBuiltIns: 'entry',
              corejs: {
                version: 3
              }
            }
          ]
        ];
      },
      plugins: [
        '@babel/plugin-proposal-nullish-coalescing-operator',
        '@babel/plugin-proposal-optional-chaining'
      ]
    },
    extend(config, ctx) {}
  },
  render: {
    csp: {
      reportOnly: false,
      hashAlgorithm: 'sha256',
      unsafeInlineCompatibility: true,
      policies: {
        'default-src': ["'self'"],
        'style-src': ["'self'", "'unsafe-inline'"],
        'script-src': ["'self'", "'unsafe-inline'", "'unsafe-eval'"]
      }
    }
  },
  router: {
    prefetchLinks: false
  },
  // publicPath: 'https://cdn.example.org/.nuxt/dist/client', // Default: '/_nuxt/'
  srcDir: 'src/client',
  telemetry: false
};
