module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  rules: {
    semi: [
      'error',
      'always'
    ],
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'always',
        named: 'never',
        asyncArrow: 'always'
      }
    ],
    'arrow-parens': [
      'error',
      'as-needed'
    ],
    indent: [
      'warn',
      2,
      {
        SwitchCase: 1,
        VariableDeclarator: 2,
        FunctionDeclaration: {
          body: 1,
          parameters: 2
        },
        FunctionExpression: {
          body: 1,
          parameters: 2
        }
      }
    ],
    'no-unused-vars': 'warn',
    'require-await': 'off'
  }
};
